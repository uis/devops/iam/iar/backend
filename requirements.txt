# Requirements for the iarbackend itself
django<3.0
psycopg2-binary
# explicitly specify django-automationcommon's git repo since changes in
# automationcommon tend to be "ad hoc" and may need testing here without a
# corresponding pypi release. Recall that git branched may be explicitly given
# in the VCS URL.
git+https://github.com/uisautomation/django-automationcommon.git@master#egg=django-automationcommon
social-auth-app-django
django-ucamlookup
django-multiselectfield
djangorestframework
django-cors-headers
drf-yasg[validation]
django-filter

#django rest framework documentation dependencies
coreapi
pygments
markdown

# For an improved python manage.py shell experience
ipython

# So that tests may be run within the container
tox

# Serving
gunicorn
