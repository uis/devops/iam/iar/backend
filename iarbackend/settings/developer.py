# Import settings from the base settings file
from .base import *  # noqa: F401, F403

DEBUG = True

INSTALLED_APPS = INSTALLED_APPS + [  # noqa: F405
    'silk',
]

MIDDLEWARE = [
    'silk.middleware.SilkyMiddleware',  # silk needs to come very high in the middleware list
] + MIDDLEWARE  # noqa: F405

STATIC_URL = '/static/'

# Ensure that logging is shown in the console.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}

# Keep track of silk's own overhead
SILKY_META = True
