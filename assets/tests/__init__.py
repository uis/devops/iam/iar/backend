from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase as DjangoTestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient


class TestCase(DjangoTestCase):
    """
    Django TestCase subclass with some useful utility methods.

    """
    def setUp(self):
        super().setUp()

        # Create a test user and client authorised as that user.
        self.user = get_user_model().objects.create_user(
            username='test0001', email='test0001@cam.ac.uk')
        self.auth_client = self.create_client(user=self.user)

        # Default values for patch_lookup()
        self.user_instids = ['TESTDEPT']
        self.user_group_names = [settings.IAR_USERS_LOOKUP_GROUP]

    def patch_lookup(self, *, email=None, instids=None, group_names=None):
        """
        Patch lookup utility functions so that the passed user email returns the passed instids and
        group names. The defaults for these being the values of self.user_instids and
        self.user_group_names. If the passed email is None, the email of self.user is user.

        Returns a callable which should be called to remove the patch.

        """
        target_user_email = email if email is not None else self.user.email

        def get_instids():
            return instids if instids is not None else self.user_instids

        def get_group_names():
            return group_names if group_names is not None else self.user_group_names

        get_instids_for_email_patcher = mock.patch(
            'assets.lookup.get_instids_for_email',
            side_effect=lambda email: get_instids() if email == target_user_email else []
        )
        get_group_names_for_email_patcher = mock.patch(
            'assets.lookup.get_group_names_for_email',
            side_effect=lambda email: get_group_names() if email == target_user_email else []
        )

        def stop():
            get_instids_for_email_patcher.stop()
            get_group_names_for_email_patcher.stop()

        get_instids_for_email_patcher.start()
        get_group_names_for_email_patcher.start()

        return stop

    def create_client(self, *, user=None):
        """
        Create an APIClient authenticated to act as the passed user. If the passed user is None or
        anonymous, the client is unauthenticated.

        """
        client = APIClient()

        if user is not None and not user.is_anonymous:
            assert user.email != ''
            token, _ = Token.objects.get_or_create(user=user)
            client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        return client
