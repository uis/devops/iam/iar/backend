# Information Asset Register Backend

This is the backend server for the [Information Asset
Register](https://iar.admin.cam.ac.uk/) of the University of Cambridge.

## License

The [LICENSE](LICENSE) file described the license under which this code is
released.

## Developer quick start

1. Copy [secrets.env.in](secrets.env.in) to `secrets.env` and set the secrets.
   Information on what secrets are required and where to get them are in
   the `secrets.env.in` file itself.
2. Start the backend via `docker-compose up devserver`.
3. Visit http://localhost:8000/ui in your browser.

You can test the application directly in the Swagger UI.

> **Note:** the Swagger UI can sometime have problems authenticating when using
> Firefox. This manifests as the header "Authorization: Bearer DUMMY_TOKEN"
> being sent no matter what login credentials are provided.

1. Click the padlock icon next to the `/token/{backend}/exchange` endpoint,
   select the "email" scope and click "Authorize". Log in with your @cam.ac.uk
   GSuite account.
2. Expand the `/token/{backend}/exchange` endpoint, click "try it out", enter
   "google-oauth2" as the backend and click the "execute" button.
3. The response body should include your email address and an API key, copy the
   API key to your clipboard.
4. Click the padlock icon next to the `/assets/` GET endpoint and enter "Token
   {key}" replacing "{key}" with the API key copied to your clipboard. Click the
   "authorize" button.
5. Expand the `/assets/` GET endpoint and click the "try it out" button. Click
   the "execute" button and confirm that the asset list is returned.

## Running tests

The test suite can be run via `docker-compose run --rm tox`.

## Creating an admin user

An admin user can be created via:

```console
$ docker-compose run --rm --entrypoint ./manage.py devserver createsuperuser
```

You can log in as the admin user to http://localhost:8000/admin when the
development server is running.

## Importing production SQL dumps

SQL dumps of the production database can be imported in the following way.
Firstly, the existing developer database volume needs to be removed:

```console
$ docker-compose down --volumes
```

Then start *just* the database:

```console
$ docker-compose up iar-db
```

Download and uncompress the SQL dump. Copy it to the container:

```console
$ docker cp dump.sql backend_iar-db_1:/tmp/dump.sql
```

Use the `psql` command to restore the database:

```console
$ docker-compose exec iar-db psql -U iarbackenduser -d iarbackend -f /tmp/dump.sql
```

Starting the development server via `docker-compose up devserver` will now make
use of the production data dump.
